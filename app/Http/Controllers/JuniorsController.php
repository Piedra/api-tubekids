<?php

namespace App\Http\Controllers;

use App\Junior;
use Illuminate\Http\Request;

class JuniorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $juniors = Junior::All();
        return view('juniors.index',compact('juniors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('juniors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $junior = Junior::create([
            'name' => $request->name,
            'username' => $request->username,
            'pin' => $request->pin,
            'age' => $request->age,
            'id_parent' => $request->id_parent
            
        ]);
        return response()
        ->json($junior);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Junior  $junior
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $junior = Junior::find($id);
        return response()
        ->json($junior);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Junior  $junior
     * @return \Illuminate\Http\Response
     */
    public function edit(Junior $junior)
    {
        return view('juniors.edit',compact('junior'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Junior  $junior
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $junior = Junior::find($id);
        $junior->name = $request->name != null ? $request->name : $junior->name;
        $junior->username = $request->username != null ? $request->username: $junior->username;
        $junior->pin = $request->pin != null ? $request->pin: $junior->pin;
        $junior->age = $request->age != null ? $request->age: $junior->age;
        $junior->save();
        return response()
        ->json(Junior::find($id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Junior  $junior
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $user = Junior::find($id);
        $user->delete();
    }
}
