<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Junior extends Model
{
    protected $fillable = [
        'name', 'username', 'pin','age','id_parent'
    ];
}
