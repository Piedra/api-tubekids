@extends('layouts.app')

@section('content')
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>
    </head>
    <body>
        <div class="container">
            <form method="POST" action="/api/video/{{$video->id}}">
                @method('PATCH')
                <div class="form-group">
                  <label for="exampleInputEmail1">URL</label>
                  <input type="text" name="url" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Copy the URL of the video">
                  <small id="emailHelp" class="form-text text-muted">The Url youtube give you</small>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Tittle</label>
                  <input type="text" name="tittle" class="form-control" id="exampleInputPassword1" placeholder="Tittle">
                  <input type="hidden" name="youtube" class="form-control" id="exampleInputPassword1" value="false">
                </div>
                <div class="form-check">
                  <input type="checkbox" class="form-check-input" id="exampleCheck1">
                  <label class="form-check-label" for="exampleCheck1">Youtube</label>
                </div>
                <button type="submit" class="btn btn-primary">Add</button>
              </form>
        </div>
    </body>
    </html>
@endsection