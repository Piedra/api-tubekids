@extends('layouts.app')

@section('content')
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>
    </head>
    <body>
            <div class="container">
                    <form method="POST" action="/api/junior/{{$junior->id}}">
                        @method('PATCH')
                        <div class="form-group">
                          <label for="exampleInputEmail1">Name</label>
                          <input type="text" name="name" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$junior->name}}" placeholder="Name of Junior">
                          <small id="emailHelp" class="form-text text-muted">Name</small>
                        </div>

                        <div class="form-group">
                                <label for="exampleInputEmail1">Username</label>
                                <input type="text" name="username" class="form-control" id="exampleInputEmail1" value="{{$junior->username}}" aria-describedby="emailHelp" placeholder="Username">
                                <small id="emailHelp" class="form-text text-muted">Username</small>
                        </div>

                        <div class="form-group">
                                <label for="exampleInputEmail1">Age</label>
                                <input type="text" name="age" class="form-control" id="exampleInputEmail1" value="{{$junior->age}}" aria-describedby="emailHelp" placeholder="Age">
                                <small id="emailHelp" class="form-text text-muted">Age</small>
                        </div>
                              
                        <div class="form-group">
                                <label for="exampleInputEmail1">PIN</label>
                                <input type="number" name="pin" class="form-control" id="exampleInputEmail1" value="{{$junior->pin}}" aria-describedby="emailHelp" placeholder="PIN">
                                <input type="hidden" name="id_parent" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{Auth::user()->id}}">
                                <small id="emailHelp" class="form-text text-muted">PIN</small>
                        </div>

                        <button type="submit" class="btn btn-primary">Add</button>
                      </form>
                </div>
    </body>
    </html>
@endsection