@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Juniors</h1>
            <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Nickname</th>
                        <th scope="col">Pin</th>
                        <th scope="col">Age</th>
                        <th scope="col">
                            <form method="GET" action="/junior/create">
                                <button class="btn btn-success">Add</button>
                              </form>
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach ($juniors as $jn)
                        <tr>
                          <th scope="row">{{$jn->name}}</th>
                          <th scope="row">{{$jn->username}}</th>
                          <th scope="row">{{$jn->pin}}</th>
                          <th scope="row">{{$jn->age}}</th>
                            <th>  
                            <form method="GET" action="junior/{{$jn->id}}/edit">
                                @csrf
                                <button class="btn btn-warning">Edit</button>
                              </form>
                            </th>  
                          <th scope="col">
                              <form method="POST" action="junior/{{$jn->id}}">
                                    @method('DELETE')
                                    @csrf
                                <button class="btn btn-danger">Delete</button>
                              </form>
                          </th>
                        </tr>
                        @endforeach
                    </tbody>
                  </table>
    </div>
@endsection