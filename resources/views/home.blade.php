@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Videos</div>
                <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Video</th>
                        <th scope="col">
                            <form method="GET" action="/video/create">
                                <button class="btn btn-success">Add</button>
                              </form>
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach ($videos as $video)
                        <tr>
                          <th scope="row">{{$video->tittle}}</th>
                          <th><iframe width="420" height="345" src="{{$video->url}}"></th>
                            <th>  
                            <form method="GET" action="video/{{$video->id}}/edit">
                                @csrf
                                <button class="btn btn-warning">Edit</button>
                              </form>
                            </th>  
                          <th scope="col">
                              <form method="POST" action="video/{{$video->id}}">
                                    @method('DELETE')
                                    @csrf
                                <button class="btn btn-danger">Delete</button>
                              </form>
                          </th>
                        </tr>
                        @endforeach
                    </tbody>
                  </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
